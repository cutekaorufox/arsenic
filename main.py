import os
import discord
import json
from discord.ext import commands

config = json.loads(open('./config.json').read())
bot = commands.Bot(command_prefix=f'{config["prefix"]}',  help_command=None)

@bot.event
async def on_ready():
    print("Bot is on!!!!\n======================")

for files in os.listdir("./cogs"):
    if files.endswith(".py"):
        bot.load_extension(f'cogs.{files[:-3]}')

bot.run(config['token'])